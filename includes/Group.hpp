#pragma once
#include <vector>
#include <cereal/cereal.hpp>
#include <cereal/types/polymorphic.hpp>
#include "types.hpp"
class Agent;
class GroupImp;

class Group
{
public:

	enum ProblemFlag
	{
		ASSIGNMENT
	};

	enum MeasureFlag
	{
		TIME,		/// Time in seconds
		DISTANCE	/// Distance in meters
	};

	enum LayerFlag
	{
		HW,  /// Between Home and work
		HM,  /// Between Home and Market
		WM   /// Between Work and Market
	};

    static std::shared_ptr<Group> Create();

	virtual 					~Group()=default;

	virtual void 			   	init(const std::vector<gps>& homes,
									 const std::vector<gps>& works) = 0;
	virtual std::vector<Agent> 	getList() = 0;
	virtual void 			   	setList(const std::vector<Agent>& data) = 0;
	virtual Agent 			   	getAgent(const std::string& name) = 0;
	virtual void 			   	removeAgent(const std::string& name) = 0;
	virtual void 				addAgent(const Agent& agent) = 0;
	virtual void 				reset() = 0;
	virtual void 				clean(std::vector<Agent>& list) = 0;
	virtual void 				update(const ProblemFlag pb, const MeasureFlag mflag, const LayerFlag pflag) = 0;
	virtual void 				setCostMatrix(const std::vector<std::vector<cost>>& matrix, const MeasureFlag mflag, const LayerFlag pflag)  = 0;
	virtual std::vector<std::vector<cost>>
								getCostMatrix(const MeasureFlag mflag, const LayerFlag pflag)  = 0;


	template<class Archive>
	void save(Archive & ar) const
	{
		ar(n);
	}

	template<class Archive>
	void load(Archive & ar)
	{
		ar(n);
	}

	int							n;

protected:
	Group()=default;
};
