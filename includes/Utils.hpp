#pragma once

#include <fstream>
#include <iostream>
#include <tuple>
#include <sys/time.h>

#include <opencv2/core/core.hpp>

#include <cereal/cereal.hpp>
#include <cereal/types/memory.hpp>
#include <cereal/archives/binary.hpp>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/vector.hpp>

#include "types.hpp"
#include "Group.hpp"

#define EARTH_RADIUS_KM 6371.0

class Utils
{
public:

	Utils()=default;
	~Utils()=default;

	static double 	DistanceFromGPS (const double lat1d, const double lon1d, const double lat2d, const double lon2d);
	static double 	Deg2Rad(double deg) { return (deg * M_PI / 180);}
	static double 	Rad2Deg(double rad) { return (rad * 180 / M_PI);}
	static void 	DrawPair(cv::Mat& map, const cv::Point& p1, const cv::Point& p2, cv::Scalar& linecolor);
	static bool 	PointInTriangle (const cv::Point2f& pt, const cv::Point2f& v1, const cv::Point2f& v2, const cv::Point2f& v3);
	static void 	FillTriangle(cv::Mat& frame, const cv::Vec6f& triangle, const cv::Scalar& color);
	static void 	BinarizeByValue (const cv::Mat& image, cv::Mat& binarized, const cv::Scalar& value);
	static void 	BinarizeMap(const cv::Mat& toBinarize, cv::Mat& binarized, const std::vector<cv::Scalar>& redZonesColors);
	static void 	ClearArea(cv::Mat& map, cv::Rect zone = cv::Rect());
	static float 	Sign (const cv::Point2f& p1, const cv::Point2f& p2, const cv::Point2f& p3);
	static double 	fRand(double fMin, double fMax);
	static double 	stodpre(std::string const &str, std::size_t const p);
};

//////////////////////////////////////////////////////////////////////
class OsTimer
{
private:
	struct timeval t0;
	struct timeval t1;

public:

OsTimer()
{
}

~OsTimer()
{
}

inline void start()
{
	gettimeofday(&t0 , NULL);
}

inline void stop()
{
	gettimeofday(&t1 , NULL);
}

inline double elapsedTime()
{
	return (t1.tv_sec-t0.tv_sec)*1000.0 + (t1.tv_usec-t0.tv_usec)/1000.0;
}

};
//////////////////////////////////////////////////////////////////////


//template<typename T>
//std::ostream &operator <<(std::ostream &os, const std::vector<T> &vec)
//{
//   std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(os, "\n"));
//   return os;
//}
//
//template<typename T>
//std::ostream &operator <<(std::ostream &os, const std::vector<std::vector<T>>& vec)
//{
//   for(auto item : vec) os << item << "\n";
//   return os;
//}
//
//template<typename T>
//std::istream &operator >>(std::istream &is, std::vector<T> &vec)
//{
//   std::copy(std::istream_iterator<T>(is), std::istream_iterator<T>(), std::back_inserter(vec)); //push_back mode
//   return is;
//}
//
//template<typename T>
//std::istream &operator >>(std::istream &is, std::vector<std::vector<T>>& vec)
//{
//	std::vector<T> tmp;
//	while(is >> tmp)
//	{
//		vec.push_back(tmp);
//		tmp.clear();
//	}
//   return is;
//}

//template<typename T1, typename T2>
//std::ostream &operator <<(std::ostream &os, const std::pair<T1,T2>& p)
//{
//   os << "(" << p.first << "," << p.second << ")" << "\n";
//   return os;
//}
//
//template<typename T1, typename T2>
//std::istream &operator >>(std::istream &is, std::pair<T1,T2> &p)
//{
//   std::copy(std::istream_iterator<T>(is), std::istream_iterator<T>(), std::back_inserter(vec)); //push_back mode
//   return is;
//}
