#pragma once
#include <vector>
#include <memory>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphml.hpp>

using boolean = int;
using row	  = int;
using col	  = int;
using cost	  = int;
using path = std::vector<int>;

struct VertexProps
{
	std::string rawcode;
	std::string rawtype;
	std::string rawosmID;
	std::string rawylatitude;
	std::string rawxlongitude;

	int 		osmID;
	double 		ylatitude;
	double		xlongitude;
	int			x;
	int			y;
};

struct EdgeProps
{
	std::string rawlabel;
	std::string rawcode;
	int			rawkey;
	std::string rawtype;
	std::string rawosmID;
	std::string rawlength;
	std::string rawmaxspeed;
	std::string rawoneway;
	std::string rawwidth;
	std::string rawservice;
	std::string rawlanes;
	std::string rawaccess;
	std::string rawtunnel;
	std::string rawbridge;
	std::string rawgeometry;
	std::string rawest_width;
	std::string rawarea;
	std::string rawlanduse;
	std::string rawbicycle;
	std::string rawfoot;
	std::string rawmotor_vehicle;
	std::string rawmotorcar;


	int 		osmID;
	double 		length;
	int 		maxspeed;
	bool 		oneway;
	// highway
	bool 		motor;
	bool 		footway;
	bool 		cycleway;
	bool		pedestrian;
	bool		track;
	bool 		path;
	bool		steps;
	bool 		bridleway;
	bool		residential;
	bool		service;
	bool		secondary;
	bool 		tertiary;
	bool 		proposed;
	bool 		construction;
	bool 		abandoned;
	bool 		platform;
	bool 		raceway;
	// access
	bool 		private_access;
	// bicycle
	bool 		nobicycle;
	// foot
	bool 		nofoot;
	// motor_vehicle
	bool 		nomotorvehicle;
	// motor_car
	bool 		nomotorcar;
	// service
	bool 		parking;
	bool 		parking_aisle;
	bool 		driveway_service;
	bool 		private_service;
	bool 		emergency_access;
	// area
	bool		area;
};

struct GraphProps
{
	std::string name;
//	std::string rawcrs;
	int 		nbOfVertices;
};

using OSMGraph = boost::adjacency_list<boost::listS, // faster than vectors
									   boost::vecS,
									   boost::bidirectionalS,
									   VertexProps,
									   EdgeProps,
									   GraphProps,
									   boost::listS>;

using OSMUndirectedGraph = boost::adjacency_list<boost::setS, // setS allows graph to be undirected (make sure of this: TODO)
											   boost::vecS,
											   boost::undirectedS,
											   VertexProps,
											   EdgeProps,
											   GraphProps,
											   boost::listS>;

using OSMGraphProps = boost::dynamic_properties;

using vertex_t = boost::graph_traits<OSMGraph>::vertex_descriptor;

using edge_t   = boost::graph_traits<OSMGraph>::edge_descriptor;

using gps = std::pair<double, double>;
