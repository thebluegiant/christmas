#pragma once
#include <cstring>
#include <unordered_map>
#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/polymorphic.hpp>

#include "Group.hpp"
#include "Population.hpp"

class PopulationImp : public Population
{
public:
	PopulationImp(const int n);

	~PopulationImp()=default;

	virtual void addGroup(const std::shared_ptr<Group>& g, const std::string& name);
	virtual void removeGroup(const std::string& name);
	virtual std::shared_ptr<Group> getGroup (const std::string& name);

private:

	std::unordered_map<std::string, std::shared_ptr<Group> > _pop;
};

CEREAL_REGISTER_TYPE(PopulationImp);
CEREAL_REGISTER_POLYMORPHIC_RELATION(Population, PopulationImp);
