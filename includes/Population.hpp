#pragma once
#include <Group.hpp>
#include <memory>

class Population
{
public:

	enum groupFlag
	{
		FRIENDS,
		COWORKERS,
		FAMILIES
	};

	static std::shared_ptr<Population> Create(const int n);

	virtual ~Population()=default;


	virtual void addGroup(const std::shared_ptr<Group>& g, const std::string& name) = 0;
	virtual void removeGroup(const std::string& name) = 0;
	virtual std::shared_ptr<Group> getGroup (const std::string& name) = 0;


protected:

	Population()=default;
};
