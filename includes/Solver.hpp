#pragma once

#include <utility>
#include "types.hpp"
class Agent;

//#define DEBUG_SOLVER


/* todo
 * ** Traditional
 * - Knapsack problem
 * - Hamiltonian path
 * - Floyd–Warshall algorithm
 * - circulation problem
 * ** Learning Combinatorial Optimization Algorithms over Graphs (with structure2vec !!)
 *
 */


class Solver
{
public:

	Solver() = default;
	~Solver() = default;

	static std::pair<cost,cost> Assign(std::vector<std::vector<cost>>* costMatrix, std::vector<Agent>& agentList);

};
