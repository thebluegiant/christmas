#pragma once
#include <iostream>
#include <tuple>
#include "City.hpp"
#include "Group.hpp"

//#define DEBUG_CITY

class CityImp : public City
{
public:

    CityImp(const std::string& file);

	CityImp() = default;

	~CityImp();

	virtual double 				getFeature(const std::string& featureName);
	virtual std::vector<gps>	generateRandomLocations(const std::string& type);
	virtual std::vector<gps>  	getLocations(const std::string& type);
	virtual void 				update();

	static int 		LoadFromGraphml (const std::string& cityFilePath, OSMGraph& graph, OSMGraphProps& dp);
	static int 		AddCustomGraphProps (OSMGraph& graph);
	static int 		CopyGraph (const OSMGraph& iGraph, OSMUndirectedGraph& oGraph);
	static int 		SaveToGraphml (const std::string& cityFilePath, const OSMGraph& graph, const OSMGraphProps& dp);
	static float 	ComputeAvgLatitude   (const OSMUndirectedGraph& graph);
	static float 	ComputeAvgLongitude  (const OSMUndirectedGraph& graph);
	static double 	ComputeTypeNetLength (const OSMUndirectedGraph& graph, const std::string& highwayType = "all_private");
	static int 		GenerateRandomLocationsFromEdge (const OSMUndirectedGraph& graph, const OSMUndirectedGraph::edge_descriptor& edge, std::vector<gps>& locations);

protected:

	OSMGraph					_osmCityGraph;
	OSMUndirectedGraph			_osmCityUndirectedGraph;
	OSMGraphProps				_osmCityGraphProps;
	int							_nbOfIntersections;
	int							_nbOfRoads;
	float						_avgLongitude;
	float 						_avgLatitude;

	double 						_totalNetLength;
	double 						_bikableNetLength;
	double 						_drivableNetLength;
	double 						_walkableNetLength;
	double 						_bikeNetLength;
	double						_pedestrianNetLength;
	double						_residentialNetLength;
	double 						_serviceNetLength;

	int					 		_nbOfHomeLocations;
	int					 		_nbOfWorkLocations;
	std::vector<gps> 			_homelocations;
	std::vector<gps> 			_worklocations;
};
