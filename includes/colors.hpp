#pragma once
#include <opencv2/core/core.hpp>

const cv::Scalar white	(255,255,255);
const cv::Scalar green  (0,255,0);
const cv::Scalar yellow (0,255,255);
const cv::Scalar black (0,0,0);
const cv::Scalar red (0,0,255);
const cv::Scalar blue (255,0,0);
const cv::Scalar brown (0,51,102);


