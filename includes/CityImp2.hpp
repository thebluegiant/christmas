#pragma once
#include <iostream>
#include "City.hpp"

//#define DEBUG_CITY2


class CityImp2 : public City
{
public:

	CityImp2(const std::string& file);

	CityImp2() = default;

	~CityImp2() = default;

	virtual double 				getFeature(const std::string& featureName) {};
	virtual std::vector<gps>	generateRandomLocations(const std::string& type) {};
	virtual std::vector<gps>  	getLocations(const std::string& type) {};
	virtual void 				update() {}; 	// todo: management of events, taxes, prices, seasonality, etc.. (use observer design)

protected:


//	OSMGraph					_osmCityGraph;
//	OSMUndirectedGraph			_osmCityUndirectedGraph;
//	OSMGraphProps				_osmCityGraphProps;
//	int							_nbOfIntersections;
//	int							_nbOfRoads;
//	float						_avgLongitude;
//	float 						_avgLatitude;
//
//	double 						_totalNetLength;
//	double 						_bikableNetLength;
//	double 						_drivableNetLength;
//	double 						_walkableNetLength;
//	double 						_bikeNetLength;
//	double						_pedestrianNetLength;
//	double						_residentialNetLength;
//	double 						_serviceNetLength;
//
//	int					 		_nbOfHomeLocations;
//	int					 		_nbOfWorkLocations;
//	std::vector<gps> 			_homelocations;
//	std::vector<gps> 			_worklocations;
};
