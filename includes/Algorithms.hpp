#pragma once
#include "types.hpp"


struct edge   { int to, length; };
struct result { int distance; path p; };

class Algorithms
{

public:
	Algorithms() = default;
	~Algorithms() = default;

	static result 	Dijkstra(const std::vector<std::vector<edge>> &graph, const int source, const int target/*, const int featureId = 0*/);
	static cost  	LapJV (const std::vector<std::vector<cost>>& assigncost, std::vector<row>& rowsol, std::vector<col>& colsol);
};
