#pragma once

struct LayerData
{
	std::vector<std::vector<cost>> 	timeCostMatrix;
	std::vector<std::vector<cost>> 	distanceCostMatrix;
	cost							totalTimePrevCostAssignment;
	cost							totalDistancePrevCostAssignment;
	cost							totalTimeNewCostAssignment;
	cost							totalDistanceNewCostAssignment;

	LayerData()
	{
		init();
	}

	void init()
	{
		timeCostMatrix.clear();
		distanceCostMatrix.clear();
		totalTimePrevCostAssignment = 0;
		totalDistancePrevCostAssignment = 0;
		totalTimeNewCostAssignment = 0;
		totalDistanceNewCostAssignment = 0;
	}

	template<class Archive>
	void save(Archive & ar) const
	{
		ar(timeCostMatrix, distanceCostMatrix,
				totalTimePrevCostAssignment, totalDistancePrevCostAssignment,
				totalTimeNewCostAssignment, totalDistanceNewCostAssignment);
	}

	template<class Archive>
	void load(Archive & ar)
	{
		ar(timeCostMatrix, distanceCostMatrix,
				totalTimePrevCostAssignment, totalDistancePrevCostAssignment,
				totalTimeNewCostAssignment, totalDistanceNewCostAssignment);
	}
};
