#pragma once

#include <string>
#include "types.hpp"
class Agent;
class Group;


class City
{
public:

    static std::shared_ptr<City> Create(const std::string& file, const int mode = 2);

	virtual ~City()=default;

	virtual double 				getFeature(const std::string& featureName) = 0;
	virtual std::vector<gps>  	generateRandomLocations(const std::string& type) = 0;
	virtual std::vector<gps> 	getLocations(const std::string& type) = 0;
	virtual void 				update() = 0;

protected:
	City()=default;
};
