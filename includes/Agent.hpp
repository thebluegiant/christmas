#pragma once
#include "types.hpp"

#include "cereal/types/utility.hpp" // cereal for std::pair


/*
 * NOTES
 * -----
 * Big Five Personality Model
 * --------------------------
 * EXTRAVERSION or positive emotion, dominance and enthusiasm
 * OPENESS to Experience or creativity, artistic sensibility, and philosophical-mindedness
 * EMOTIONAL STABILITY or the absence of negative emotion (anxiety, emotional pain, shame and guilt)
 * CONSCIENTIOUSNESS or orderliness and persistence
 * AGREEABLENESS or warmth, empathy and tender-mindedness (versus assertiveness and aggression)
 *
 */


class Agent
{
public:
	Agent()=default;

	~Agent()=default;

	Agent(const std::string& name, const gps& home, const gps& work): name(name),
																	  worklocation(work),
																	  homelocation(home),
																	  commutetime(INT_MAX),
																	  commutedistance(INT_MAX),
																	  error(false)
	{

	}

	bool 		error;
	std::string name;
	gps 		worklocation;
	gps 		homelocation;

	std::string homeaddress;
	std::string workaddress;
	int 		commutetime;
	int 		commutedistance;

	template<typename Archive>
	void save(Archive & ar) const
	{
		ar(name,
		   worklocation, homelocation,
		   homeaddress, workaddress,
		   commutetime, commutedistance);
	}

	template<typename Archive>
	void load(Archive & ar)
	{
		ar(name,
		   worklocation, homelocation,
		   homeaddress, workaddress,
		   commutetime, commutedistance);
	}
};
