#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

#include "Group.hpp"
#include "GroupImp.hpp"
#include "opaque_types.hpp"


class PyGroup : public Group
{
public:
    // Trampoline (need one for each virtual function)
	virtual void 			   	init(const std::vector<gps>& homes, const std::vector<gps>& works)
								override { PYBIND11_OVERLOAD_PURE(void, Group, init, homes, works);};
	virtual std::vector<Agent> 	getList() override { PYBIND11_OVERLOAD_PURE(std::vector<Agent>, Group, getList);};
	virtual void 			   	setList(const std::vector<Agent>& data) override { PYBIND11_OVERLOAD_PURE(void, Group, setList, data);};
	virtual Agent 			   	getAgent(const std::string& name) override { PYBIND11_OVERLOAD_PURE(Agent, Group, getAgent, name);};
	virtual void 			   	removeAgent(const std::string& name) override { PYBIND11_OVERLOAD_PURE(void, Group, removeAgent, name);};
	virtual void 				addAgent(const Agent& agent) override { PYBIND11_OVERLOAD_PURE(void, Group, addAgent, agent);};
	virtual void 				reset() override { PYBIND11_OVERLOAD_PURE(void, Group, reset);};
	virtual void 				clean(std::vector<Agent>& list) override { PYBIND11_OVERLOAD_PURE(void, Group, clean, list);};
	virtual void 				update(const ProblemFlag pb, const MeasureFlag mflag, const LayerFlag pflag)
								override { PYBIND11_OVERLOAD_PURE(void, Group, update, pb, mflag, pflag);};
	virtual void 				setCostMatrix(const std::vector<std::vector<cost>>& matrix, const MeasureFlag mflag, const LayerFlag pflag)
								override { PYBIND11_OVERLOAD_PURE(void, Group, setCostMatrix, matrix, mflag, pflag);};
	virtual std::vector<std::vector<cost>>
								getCostMatrix(const MeasureFlag mflag, const LayerFlag pflag)
								override { PYBIND11_OVERLOAD_PURE(std::vector<std::vector<cost>>, Group, getCostMatrix);};
};
