#pragma once
#include "Group.hpp"
#include "Agent.hpp"
#include "Utils.hpp"
#include <cstring>
#include "types.hpp"

#include <cereal/archives/portable_binary.hpp>
#include <cereal/types/vector.hpp>
#include <cereal/types/polymorphic.hpp>

#include "LayerData.hpp"

#define DEBUG_GROUP


class GroupImp : public Group
{
public:

	GroupImp();
	~GroupImp()=default;

	void 			   	init(const std::vector<gps>& homes, const std::vector<gps>& works);
	std::vector<Agent> 	getList();
	void 			   	setList(const std::vector<Agent>& data);
	Agent 			   	getAgent(const std::string& name);
	void 			   	removeAgent(const std::string& name);
	void 				addAgent(const Agent& agent);
	void 				reset();
    void 				clean(std::vector<Agent>& list);

	void 				update(const ProblemFlag pb, const MeasureFlag mflag, const LayerFlag pflag);

	void 				setCostMatrix(const std::vector<std::vector<cost>>& matrix,
									  const MeasureFlag mflag, const LayerFlag pflag);

	std::vector<std::vector<cost>> 	getCostMatrix(const MeasureFlag mflag, const LayerFlag pflag);


	template<class Archive>
	void save(Archive & ar) const
	{
		ar(cereal::base_class<Group>(this));
		ar(_listOfAgents, _initialListOfAgents, _previousListOfAgents,
		   _HW);
	}

	template<class Archive>
	void load(Archive & ar)
	{
		ar(cereal::base_class<Group>(this));
		ar(_listOfAgents, _initialListOfAgents, _previousListOfAgents,
		   _HW);
	}

protected:
    std::vector<Agent> 				_listOfAgents;
    std::vector<Agent>				_previousListOfAgents;
    std::vector<Agent>				_initialListOfAgents;

    LayerData						_HW;
};

CEREAL_REGISTER_TYPE(GroupImp);
CEREAL_REGISTER_POLYMORPHIC_RELATION(Group, GroupImp);

