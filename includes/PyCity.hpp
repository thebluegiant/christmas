#pragma once

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

#include "opaque_types.hpp"
#include "City.hpp"
#include "CityImp.hpp"

class PyCity : public City
{
public:
    // Trampoline (need one for each virtual function)
    double 				getFeature(const std::string& str) override { PYBIND11_OVERLOAD_PURE(double, City, getFeature, str); }
    std::vector<gps> 	generateRandomLocations(const std::string& type) override { PYBIND11_OVERLOAD_PURE(std::vector<gps>, City, generateRandomLocations, type); }
    std::vector<gps> 	getLocations(const std::string& type) override { PYBIND11_OVERLOAD_PURE(std::vector<gps>, City, getLocations, type); }
    void 				update() override { PYBIND11_OVERLOAD_PURE(void, City, update);}
};
