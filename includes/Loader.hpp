#pragma once
#include <cereal/cereal.hpp>

class Loader
{
public:

	Loader()=default;
	~Loader()=default;

	template<typename CType>
	static void SaveBin(const std::string& filename, std::shared_ptr<CType> ptr)
	{
		std::ofstream os(filename, std::ios::binary);
		cereal::PortableBinaryOutputArchive archive(os);
		archive(ptr);
	}

	template<typename CType>
	static std::shared_ptr<CType> LoadBin(const std::string& filename)
	{
		std::ifstream is(filename, std::ios::binary);
		cereal::PortableBinaryInputArchive archive(is);
	    std::shared_ptr<CType> ptr;
		archive(ptr);
		return ptr;
	}
};
