import sys
sys.path.insert(0, '../../Debug')
import libcmas as cmas
import libcmas.city as ct
import libcmas.group as gp
import libcmas.utils as ut
import libcmas.loader as ld


import osmnx as ox
import urllib
import json
import googlemaps


#  Input:  [1] G = City Graphml 
#          [2] N = Number of agents / work-home pairs
#          [3] mode = time | distance
#  Output: [1] file = binary file containing all Agents data
#
#  ex: python genHomeWorkData.py city.graphml 10 filename.bin 


gmaps = googlemaps.Client(key='YOUR_API_KEY')
serviceurl = 'https://maps.googleapis.com/maps/api/distancematrix/json?'

# Look up an address with reverse geocoding
def getAddress(gps):
    geocode = gmaps.reverse_geocode((gps[1],gps[0]))
    address = geocode[0]['formatted_address']
    if "Unamed Road" not in address: return address
    else:  return "error"

js = "data" #init var

def getCost(origin, destination, mode):
    global js
    error = True
    origin = origin.encode("utf8")
    destination = destination.encode("utf8")
    if len(origin) == 0 | len(destination) == 0: print ("Empty addresses...")
    print ('origin' , origin)
    print ('destination' , destination)
    # reaching server
    url = serviceurl + urllib.parse.urlencode({'sensor':'false', 'origins': origin, 'destinations': destination, 'mode': mode, 'key': 'YOUR_API_KEY'})
    print (url)
    uh = urllib.request.urlopen(url)
    data = uh.read()
    
    duration = 0
    distance = 0
    try:    
        js = json.loads(data)
        duration = js["rows"][0]["elements"][0]["duration"]["value"]
        distance = js["rows"][0]["elements"][0]["distance"]["value"]
        error = False
    except:
        pass
        
    if 'status' not in js or js['status'] != 'OK':
        print ('==== Failure To Retrieve ====')
        print (data)
     
    return [distance, duration, error]



G        = sys.argv[1]       # .graphml file
N        = int(sys.argv[2])  # int ex: 10, 100
binaryfile = sys.argv[3]     # output binary file

# Reading city
city  = ct.create(G, 1)
# generating random home and work places dataset
homeplaces = city.generateRandomLocations('home')
workplaces =  city.generateRandomLocations('work')

# creating group of people
people = gp.create()
people.size = N
people.init(homeplaces, workplaces)

agents = people.getList()

# Generate address from gps coordinates
# addresses = []
for agent in agents:
    origin = getAddress(agent.homelocation)
    destination = getAddress(agent.worklocation)
    if origin == "error" or destination == "error":
        agent.error = True
    # update agent
    agent.homeaddress = origin
    agent.workaddress = destination

    
# cleaning agents data
people.clean(agents)

print (len(agents) , '/', N , 'agents left (1)')

print ('Computing original costs (time + distance) ...')
for agent in agents:
    [dist, time, error] = getCost(agent.homeaddress, agent.workaddress, mode="driving")
    print ('original costs : distance=' , dist , ' , time=' , time)
    agent.commutetime = time
    agent.commutedistance = dist
    agent.error = error
    
# cleaning agents data
people.clean(agents)

print (len(agents) , '/', N , 'agents left (2)')

print ('Computing cost matrices (time + distance) ...')
distMatrix = []
timeMatrix = []
for agenth in agents:
    distVector = []
    timeVector = []
    for agentw in agents:
        [dist, time, error] = getCost(agenth.homeaddress, agentw.workaddress, mode="driving")
        distVector.append(dist)
        timeVector.append(time)
        print ('Duration=',time, ' & ' , 'Distance=',dist)
    distMatrix.append(distVector)
    timeMatrix.append(timeVector)

# updating (setting cost matrices)
people.setCostMatrix(timeMatrix, gp.measure.time, gp.layer.hw)
people.setCostMatrix(distMatrix, gp.measure.distance, gp.layer.hw)

print ('Saving Agents object ... ')
ld.saveGroup(binaryfile, people)
print ('[DONE]')
    
