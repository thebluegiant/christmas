import sys
sys.path.insert(0, '../Debug')

import libcmas.city as ct
import libcmas.group as gp
import libcmas.loader as ld

ibinaryfile = sys.argv[1]
obinaryfile_time = sys.argv[2]

people = ld.loadGroup(ibinaryfile)
people.update(gp.pb.assignment, gp.measure.time, gp.layer.hw)
ld.saveGroup(obinaryfile_time, people)


#obinaryfile_dist = sys.argv[2]
#people = ld.loadGroup(ibinaryfile)
#people.update(gp.pb.assignment, gp.measure.distance, gp.layer.hw)
#ld.saveGroup(obinaryfile_dist, people)
