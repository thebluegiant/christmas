#include "City.hpp"
#include "CityImp.hpp"
#include "CityImp2.hpp"
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/stl_bind.h>

//
//

/////////////////////////////////////////////////////////////////////
/// Factory
/// @param[in] file Path to graphml file
/// @return City object
/////////////////////////////////////////////////////////////////////
std::shared_ptr<City> City::Create(const std::string& file, const int mode)
{
	std::cout << "Loading city network from : " << file << std::endl;

	if (mode == 2)
		return std::make_shared<CityImp2>(file);
	return std::make_shared<CityImp>(file);
}
