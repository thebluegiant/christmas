#include "PyCity.hpp"
#include "PyGroup.hpp"
#include <cereal/types/polymorphic.hpp>
#include <memory>
#include "Loader.hpp"

namespace py = pybind11;


PYBIND11_MODULE(libcmas, m)
{
    m.doc() = "City Multi-Agent Platform"; // docstring

    py::bind_vector<std::vector<gps>>(m, "gpslist");
    py::bind_vector<std::vector<Agent>>(m, "agentlist");
    py::bind_vector<std::vector<std::vector<float>>>(m, "floatlistlist");

    py::module mutils = m.def_submodule("utils");
    py::module mcity  = m.def_submodule("city");
    py::module mgroup = m.def_submodule("group");
    py::module magent = m.def_submodule("agent");
    py::module mloader = m.def_submodule("loader");

    // utilities
	mloader.def("loadGroup", &Loader::LoadBin<Group>);
	mloader.def("saveGroup", &Loader::SaveBin<Group>);
	mloader.def("loadCity", &Loader::LoadBin<City>);
	mloader.def("saveCity", &Loader::SaveBin<City>);

    // Factories
    mcity.def("create", &City::Create);
    mgroup.def("create", &Group::Create);

    // Agent
    py::class_<Agent> agent(magent, "agent");
    agent
	 	 .def(py::init<>())
    	 .def(py::init<const std::string&, const gps&, const gps&>())
    	 .def_readwrite("error", &Agent::error)
    	 .def_readwrite("name", &Agent::name)
    	 .def_readwrite("homelocation", &Agent::homelocation)
    	 .def_readwrite("worklocation", &Agent::worklocation)
		 .def_readwrite("homeaddress", &Agent::homeaddress)
		 .def_readwrite("workaddress", &Agent::workaddress)
		 .def_readwrite("commutetime", &Agent::commutetime)
		 .def_readwrite("commutedistance", &Agent::commutedistance);

    // City
    py::class_<City, PyCity, std::shared_ptr<City> > city(mcity, "city");
    city
        .def(py::init<>())
    	.def("getFeature", &City::getFeature)
    	.def("generateRandomLocations", &City::generateRandomLocations)
    	.def("getLocations", &City::getLocations)
        .def("update", &City::update);

    // Group
    py::class_<Group, PyGroup, std::shared_ptr<Group>> group(mgroup, "group");
    group
    	 .def(py::init<>())
         .def("init", &Group::init)
         .def("getList", &Group::getList)
         .def("setList", &Group::setList)
         .def("getAgent", &Group::getAgent)
         .def("setCostMatrix", &Group::setCostMatrix)
         .def("removeAgent", &Group::removeAgent)
         .def("addAgent", &Group::addAgent)
         .def("reset", &Group::reset)
         .def("clean", &Group::clean)
         .def("update", &Group::update)
    	 .def_readwrite("size", &Group::n);


    // enums
    py::enum_<Group::ProblemFlag>(mgroup, "pb")
					.value("assignment", Group::ProblemFlag::ASSIGNMENT)
					.export_values();
    py::enum_<Group::MeasureFlag>(mgroup, "measure")
					.value("time", Group::MeasureFlag::TIME)
					.value("distance", Group::MeasureFlag::DISTANCE)
					.export_values();
    py::enum_<Group::LayerFlag>(mgroup, "layer")
					.value("hw", Group::LayerFlag::HW)
					.value("hm", Group::LayerFlag::HM)
					.value("wm", Group::LayerFlag::WM)
					.export_values();
}
