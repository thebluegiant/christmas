#include <boost/filesystem.hpp>
#include <boost/exception/all.hpp>
#include <boost/log/exceptions.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/graphml.hpp>

#include "CityImp.hpp"
#include "Utils.hpp"
#include "cereal/types/memory.hpp"
#include "cereal/archives/binary.hpp"
#include "cereal/archives/portable_binary.hpp"
#include "cereal/types/vector.hpp"

#include <numeric>



/////////////////////////////////////////////////////////////////////
/// Loads the city from graphml and computes important features
/// @param[in] file Path to .graphml file
/////////////////////////////////////////////////////////////////////
CityImp::CityImp(const std::string& file): _nbOfHomeLocations(0),
										   _nbOfWorkLocations(0),
										   _osmCityGraph(),
										   _osmCityUndirectedGraph(),
										   _nbOfIntersections(0),
										   _nbOfRoads(0),
										   _avgLongitude(0.f),
										   _avgLatitude(0.f),
								           City()
{
	if(boost::filesystem::extension(file) != ".graphml")
	{
		std::cerr << "This implementation only supports .graphml files." << std::endl;
		return;
	}

	LoadFromGraphml(file, _osmCityGraph, _osmCityGraphProps);
	AddCustomGraphProps(_osmCityGraph);
	CopyGraph(_osmCityGraph, _osmCityUndirectedGraph);

	_nbOfIntersections = boost::num_vertices(_osmCityUndirectedGraph);
	_nbOfRoads = boost::num_edges(_osmCityUndirectedGraph);

	_avgLatitude  = ComputeAvgLatitude(_osmCityUndirectedGraph);
	_avgLongitude = ComputeAvgLongitude(_osmCityUndirectedGraph);
	_totalNetLength = ComputeTypeNetLength(_osmCityUndirectedGraph); // all_private
	_bikeNetLength  = ComputeTypeNetLength(_osmCityUndirectedGraph, "cycleway");
	_pedestrianNetLength = 	ComputeTypeNetLength(_osmCityUndirectedGraph, "pedestrian");
	_bikableNetLength = ComputeTypeNetLength(_osmCityUndirectedGraph, "bikable");
	_walkableNetLength  = ComputeTypeNetLength(_osmCityUndirectedGraph, "walkable");
	_drivableNetLength  = ComputeTypeNetLength(_osmCityUndirectedGraph, "drivable");
	_residentialNetLength = ComputeTypeNetLength(_osmCityUndirectedGraph, "residential");
	_serviceNetLength = 	ComputeTypeNetLength(_osmCityUndirectedGraph, "service");

	std::cout << "Lat/Long: " << _avgLatitude << " / " << _avgLongitude << " & ";
	std::cout << "Nodes/Edges: " << _nbOfIntersections << " / " << _nbOfRoads << std::endl;

#ifdef DEBUG_CITY
	std::cout << "_totalNetLength:" << _totalNetLength << std::endl;
	std::cout << "_bikeNetLength: " << _bikeNetLength << std::endl;
	std::cout << "_pedestrianNetLength: " << _pedestrianNetLength << std::endl;
	std::cout << "_bikableNetLength: " << _bikableNetLength << std::endl;
	std::cout << "_walkableNetLength: " << _walkableNetLength << std::endl;
	std::cout << "_drivableNetLength: " << _drivableNetLength << std::endl;
	std::cout << "_residentialNetLength: " << _residentialNetLength << std::endl;
	std::cout << "_serviceNetLength: " << _serviceNetLength << std::endl;
#endif
}

/////////////////////////////////////////////////////////////////////
///
///
/////////////////////////////////////////////////////////////////////
CityImp::~CityImp()
{
}

/////////////////////////////////////////////////////////////////////
/// @param[in] featureName features:
///	- intersections
///	- roads
///	- longitude
///	- latitude
///	- netsize
///	- bikenetsize
///	- pedestriannetsize
///	- bikablenetsize
///	- drivablenetsize
///	- walkablenetsize
///	- residentialnetsize
///	- servicenetsize
/// @return City Feature
/////////////////////////////////////////////////////////////////////
double CityImp::getFeature(const std::string& featureName)
{
	if (featureName == "intersections") return _nbOfIntersections;
	else if (featureName == "roads") return _nbOfRoads;
	else if (featureName == "longitude") return _avgLongitude;
	else if (featureName == "latitude") return _avgLatitude;
	else if (featureName == "netsize") return _totalNetLength;
	else if (featureName == "bikenetsize") return _bikeNetLength;
	else if (featureName == "pedestriannetsize") return _pedestrianNetLength;
	else if (featureName == "bikablenetsize") return _bikableNetLength;
	else if (featureName == "drivablenetsize") return _drivableNetLength;
	else if (featureName == "walkablenetsize") return _walkableNetLength;
	else if (featureName == "residentialnetsize") return _residentialNetLength;
	else if (featureName == "servicenetsize") return _serviceNetLength;

	return 0;
}

/////////////////////////////////////////////////////////////////////
/// Randomly generate GPS locations according to city's edge labels (service/residential)
/// @param[in] type Values: home, work
/// @return Number of locations
/////////////////////////////////////////////////////////////////////
std::vector<gps> CityImp::generateRandomLocations(const std::string& type)
{
    boost::graph_traits<OSMUndirectedGraph>::edge_iterator e_it, e_end;

    for (std::tie(e_it, e_end) = boost::edges(_osmCityUndirectedGraph); e_it != e_end; ++e_it)
    {
    	if (type == "home")
    	{
    		if (_osmCityUndirectedGraph[*e_it].residential)
    			GenerateRandomLocationsFromEdge (_osmCityUndirectedGraph, *e_it, _homelocations);
    	}
    	else if (type == "work")
    	{
    		if (_osmCityUndirectedGraph[*e_it].service)
    			GenerateRandomLocationsFromEdge (_osmCityUndirectedGraph, *e_it, _worklocations);
    	}
    }

	return type == "home" ? _homelocations :
		   type == "work" ? _worklocations : std::vector<gps>();
}

/////////////////////////////////////////////////////////////////////
/// @param[in] type Values: home or work
/// @return GPS Locations
/////////////////////////////////////////////////////////////////////
std::vector<gps> CityImp::getLocations(const std::string& type)
{
	if (type == "home") return _homelocations;
	else if (type == "work") return _worklocations;
}

/////////////////////////////////////////////////////////////////////
///
///
/////////////////////////////////////////////////////////////////////
void CityImp::update()
{
	std::cout << "updating..." << std::endl;

}



/////////////////////////////////////////////////////////////////////
/// Loads graphml file in a boost graph
/// @param[in] cityFilePath Path to graphml file
/// @param[out] graph Bidirectional boost graph
/// @param[out] dp Graph dynamic properties
/////////////////////////////////////////////////////////////////////
int CityImp::LoadFromGraphml(const std::string& cityFilePath, OSMGraph& graph, OSMGraphProps& dp)
{
	if (boost::filesystem::extension(cityFilePath) != ".graphml")
	{
		std::cerr << "Error: File must have .graphml extension." << std::endl;
		return -1;
	}

	std::ifstream cityFile;
	cityFile.open(cityFilePath.c_str());

	if (!cityFile.is_open())
	{
		std::cerr << "graphml file not opened. Loading failed." << std::endl;
		return -1;
	}

	// Mapping OSM properties to graph properties
	// for vertices
	dp.property("ref",boost::get(&VertexProps::rawcode, graph));
	dp.property("highway",boost::get(&VertexProps::rawtype, graph));
	dp.property("osmid",boost::get(&VertexProps::rawosmID, graph));
	dp.property("y",boost::get(&VertexProps::rawylatitude, graph));
	dp.property("x",boost::get(&VertexProps::rawxlongitude, graph));
	// for edges
	dp.property("ref",boost::get(&EdgeProps::rawcode, graph));
	dp.property("name",boost::get(&EdgeProps::rawlabel, graph));
	dp.property("osmid",boost::get(&EdgeProps::rawosmID, graph));
	dp.property("key",boost::get(&EdgeProps::rawkey, graph));
	dp.property("highway",boost::get(&EdgeProps::rawtype, graph));
	dp.property("length",boost::get(&EdgeProps::rawlength, graph));
	dp.property("maxspeed",boost::get(&EdgeProps::rawmaxspeed, graph));
	dp.property("oneway",boost::get(&EdgeProps::rawoneway, graph));
	dp.property("width",boost::get(&EdgeProps::rawwidth, graph));
	dp.property("service",boost::get(&EdgeProps::rawservice, graph));
	dp.property("lanes",boost::get(&EdgeProps::rawlanes, graph));
	dp.property("access",boost::get(&EdgeProps::rawaccess, graph));
	dp.property("tunnel",boost::get(&EdgeProps::rawtunnel, graph));
	dp.property("bridge",boost::get(&EdgeProps::rawbridge, graph));
	dp.property("geometry",boost::get(&EdgeProps::rawgeometry, graph));
	dp.property("est_width",boost::get(&EdgeProps::rawest_width, graph));
	dp.property("area",boost::get(&EdgeProps::rawarea, graph));
	dp.property("landuse",boost::get(&EdgeProps::rawlanduse, graph));
	dp.property("bicycle",boost::get(&EdgeProps::rawbicycle, graph));
	dp.property("foot",boost::get(&EdgeProps::rawfoot, graph));
	dp.property("motor_vehicle",boost::get(&EdgeProps::rawmotor_vehicle, graph));
	dp.property("motorcar",boost::get(&EdgeProps::rawmotorcar, graph));
	// for graph TODO
//	dp.property("name",boost::get(&GraphProps::name, graph));
//	dp.property("crs", boost::get(&GraphProps::rawcrs, graph));

	try
	{
		boost::read_graphml(cityFile, graph, dp);
	}
	catch(boost::exception const&  ex)
	{
        std::string diag = boost::diagnostic_information(ex);
        std::cout << std::endl << "Exception when running boost::read_graphml : " << diag << std::endl;
	}

	return 0;
}


/////////////////////////////////////////////////////////////////////
/// Adds metadata to graph
/// @param[out] Bidirectional boost graph
/////////////////////////////////////////////////////////////////////
int CityImp::AddCustomGraphProps (OSMGraph& graph)
{
	// Over nodes
	for (auto vp = boost::vertices(graph); vp.first != vp.second; ++vp.first)
    {
    	OSMGraph::vertex_descriptor v = *vp.first;
    	graph[v].osmID = atoi(graph[v].rawosmID.c_str());
    	graph[v].ylatitude  = Utils::stodpre(graph[v].rawylatitude, 16);
    	graph[v].xlongitude = Utils::stodpre(graph[v].rawxlongitude, 16);
    }

    // Over egdes
    boost::graph_traits<OSMGraph>::edge_iterator e_it, e_end;
    for(std::tie(e_it, e_end) = boost::edges(graph); e_it != e_end; ++e_it)
    {
    	graph[*e_it].osmID = atoi(graph[*e_it].rawosmID.c_str());

    	graph[*e_it].length = (double) atof(graph[*e_it].rawlength.c_str());

    	graph[*e_it].maxspeed = atoi(graph[*e_it].rawmaxspeed.c_str());

    	graph[*e_it].oneway = graph[*e_it].rawoneway == "True" ? true : false;

    	graph[*e_it].area = graph[*e_it].rawarea == "yes" ? true : false;

    	graph[*e_it].private_access = graph[*e_it].rawaccess.find("private") != std::string::npos ? true : false;

    	graph[*e_it].nobicycle = graph[*e_it].rawbicycle == "no" ? true: false;

    	graph[*e_it].nofoot = graph[*e_it].rawfoot == "no" ? true: false;

    	graph[*e_it].nomotorcar = graph[*e_it].rawmotorcar== "no" ? true: false;

    	graph[*e_it].nomotorvehicle = graph[*e_it].rawmotor_vehicle == "no" ? true: false;

    	graph[*e_it].motor = graph[*e_it].rawtype.find("motor") != std::string::npos ? true : false;
    	graph[*e_it].footway = graph[*e_it].rawtype.find("footway") != std::string::npos ? true : false;
    	graph[*e_it].cycleway = graph[*e_it].rawtype.find("cycleway") != std::string::npos ? true : false;
    	graph[*e_it].bridleway = graph[*e_it].rawtype.find("bridleway") != std::string::npos ? true : false;
    	graph[*e_it].pedestrian = graph[*e_it].rawtype.find("pedestrian") != std::string::npos  ? true : false;
    	graph[*e_it].steps = graph[*e_it].rawtype.find("steps") != std::string::npos ? true : false;
    	graph[*e_it].track = graph[*e_it].rawtype.find("track") != std::string::npos ? true : false;
    	graph[*e_it].path = graph[*e_it].rawtype.find("path") != std::string::npos ? true : false;
    	graph[*e_it].residential = graph[*e_it].rawtype.find("residential") != std::string::npos ? true : false;
    	graph[*e_it].service = graph[*e_it].rawtype.find("service") != std::string::npos ? true : false;
    	graph[*e_it].secondary = graph[*e_it].rawtype.find("secondary") != std::string::npos ? true : false;
    	graph[*e_it].tertiary = graph[*e_it].rawtype.find("tertiary") != std::string::npos ? true : false;
    	graph[*e_it].proposed = graph[*e_it].rawtype.find("proposed") != std::string::npos ? true : false;
    	graph[*e_it].construction = graph[*e_it].rawtype.find("construction") != std::string::npos ? true : false;
    	graph[*e_it].abandoned = graph[*e_it].rawtype.find("abandoned") != std::string::npos ? true : false;
    	graph[*e_it].platform = graph[*e_it].rawtype.find("platform") != std::string::npos ? true : false;
    	graph[*e_it].raceway = graph[*e_it].rawtype.find("raceway") != std::string::npos ? true : false;

    	graph[*e_it].parking = graph[*e_it].rawservice.find("parking") != std::string::npos ? true : false;
    	graph[*e_it].parking_aisle = graph[*e_it].rawservice.find("parking_aisle") != std::string::npos ? true : false;
    	graph[*e_it].driveway_service = graph[*e_it].rawservice.find("driveway") != std::string::npos ? true : false;
    	graph[*e_it].emergency_access = graph[*e_it].rawservice.find("emergency_access") != std::string::npos ? true : false;
    	graph[*e_it].private_service = graph[*e_it].rawservice.find("private") != std::string::npos ? true : false;
    }

    return 0;
}

/////////////////////////////////////////////////////////////////////
/// Copies bidirectional graph to an undirected one
/// @param[in] iGraph Bidirectional boost graph
/// @param[out] oGraph Undirected boost graph
/////////////////////////////////////////////////////////////////////
int CityImp::CopyGraph (const OSMGraph& iGraph, OSMUndirectedGraph& oGraph)
{
	boost::copy_graph(iGraph, oGraph);
	return 0;
}


/////////////////////////////////////////////////////////////////////
/// Writes the bidirectional graph with its dynamic properties to graphml file
/// @param[in] cityFilePath Bidirectional boost graph
/// @param[in] graph Bidirectional boost graph
/// @param[in] dp Dynamic properties
/////////////////////////////////////////////////////////////////////
int CityImp::SaveToGraphml (const std::string& cityFilePath, const OSMGraph& graph, const OSMGraphProps& dp)
{
	if (boost::filesystem::extension(cityFilePath) != ".graphml")
	{
		std::cerr << "Error: File must have .graphml extension." << std::endl;
		return -1;
	}

	std::ofstream cityFile;
	cityFile.open(cityFilePath.c_str());

	if (!cityFile.is_open())
	{
		std::cerr << "City file not opened. Failed saving." << std::endl;
		return -1;
	}

	try
	{
		boost::write_graphml(cityFile, graph, dp);
	}
	catch(boost::exception const&  ex)
	{
        std::string diag = boost::diagnostic_information(ex);
        std::cout << std::endl << "Exception when running boost::write_graphml : " << diag << std::endl;
	}

	return 0;
}


/////////////////////////////////////////////////////////////////////
/// Computes average city latitude
/// @param[in] graph Undirected boost graph
/////////////////////////////////////////////////////////////////////
float CityImp::ComputeAvgLatitude (const OSMUndirectedGraph& graph)
{
	std::vector<double> lats;
    for (auto vp = boost::vertices(graph); vp.first != vp.second; ++vp.first)
    {
    	OSMUndirectedGraph::vertex_descriptor v = *vp.first;
    	lats.push_back(graph[v].ylatitude);
    }
    return (float) std::accumulate(lats.begin(), lats.end(), 0.f) / lats.size();
}

/////////////////////////////////////////////////////////////////////
/// Computes average city longitude
/// @param[in] graph Undirected boost graph
/////////////////////////////////////////////////////////////////////
float CityImp::ComputeAvgLongitude (const OSMUndirectedGraph& graph)
{
	std::vector<double> longs;
    for (auto vp = boost::vertices(graph); vp.first != vp.second; ++vp.first)
    {
    	OSMUndirectedGraph::vertex_descriptor v = *vp.first;
    	longs.push_back(graph[v].xlongitude);
    }
    return (float) std::accumulate(longs.begin(), longs.end(), 0.f) / longs.size();
}

/////////////////////////////////////////////////////////////////////
/// Computes length of a specific city network
/// @param[in] graph Undirected boost graph
/// @param[in] networkType
///	- bikable
/// - walkable
/// - drivable
/// - all
/// - all_private
///	- cycleway
/// - residential
/// - pedestrian
/// - service
/// @return Net length in meters
/////////////////////////////////////////////////////////////////////
double CityImp::ComputeTypeNetLength (const OSMUndirectedGraph& graph, const std::string& networkType)
{
	double length = 0.0;
    boost::graph_traits<OSMUndirectedGraph>::edge_iterator e_it, e_end;
    for(std::tie(e_it, e_end) = boost::edges(graph); e_it != e_end; ++e_it)
    {
    	double edgeLength = graph[*e_it].length;

    	bool isarea = graph[*e_it].area;
    	bool iscycleway = graph[*e_it].cycleway;
    	bool ismotor = graph[*e_it].motor;
    	bool isfootway = graph[*e_it].footway;
    	bool ispedestrian = graph[*e_it].pedestrian;
    	bool isresidential = graph[*e_it].residential;
    	bool isservice = graph[*e_it].service;
    	bool ispath = graph[*e_it].path;
    	bool istrack = graph[*e_it].track;
    	bool issteps = graph[*e_it].steps;
    	bool isproposed = graph[*e_it].proposed;
    	bool isconstruction = graph[*e_it].construction;
    	bool isabandoned= graph[*e_it].abandoned;
    	bool isplatform = graph[*e_it].platform;
    	bool israceway = graph[*e_it].raceway;
    	bool isbridleway = graph[*e_it].bridleway;
    	bool isnoBicycle = graph[*e_it].nobicycle;
    	bool isnoFoot = graph[*e_it].nofoot;
    	bool isnoMotorCar = graph[*e_it].nomotorcar;
    	bool isnoMotorVehicle = graph[*e_it].nomotorvehicle;
    	bool isprivateAccess = graph[*e_it].private_access;
    	bool isprivateService = graph[*e_it].private_service;
    	bool isemergencyAccess = graph[*e_it].emergency_access;
    	bool isparking = graph[*e_it].parking;
    	bool isparkingAisle = graph[*e_it].parking_aisle;
    	bool isdrivewayService = graph[*e_it].driveway_service;

    	if(networkType == "bikable")
    	{
        	if (!isarea && !isfootway && !ismotor && !isproposed && !isconstruction && !isabandoned && !isplatform &&
        		!israceway && !isnoBicycle && !isprivateAccess && !isprivateService)
        		length += edgeLength;
    	}
    	else if(networkType == "walkable")
    	{
        	if (!isarea && !iscycleway && !ismotor && !isproposed && !isconstruction && !isabandoned && !isplatform &&
        		!israceway && !isnoFoot && !isprivateAccess && !isprivateService)
        		length += edgeLength;
    	}
    	else if(networkType == "drivable")
    	{
        	if (!isarea && !iscycleway && !isfootway && !ispedestrian && !ispath && !istrack && !issteps &&
        		!isproposed && !isconstruction && !isabandoned && !isplatform && !israceway && !isservice &&
        		!isbridleway && !isnoMotorVehicle && !isnoMotorCar && !isprivateAccess && !isparking &&
        		!isparkingAisle && !isdrivewayService && !isprivateService && !isemergencyAccess)
        		length += edgeLength;
    	}
    	else if(networkType == "all") // without private
    	{
        	if (!isarea && !isproposed && !isconstruction && !isabandoned && !isplatform && !israceway &&
        		!isprivateAccess && !isprivateService)
        		length += edgeLength;
    	}
    	else if(networkType == "all_private")
    	{
        	if (!isarea && !isproposed && !isconstruction && !isabandoned && !isplatform && !israceway)
        		length += edgeLength;
    	}
    	else if(networkType == "cycleway")
    	{
        	if (graph[*e_it].cycleway) length += edgeLength;
    	}
        else if(networkType == "pedestrian")
        {
        	if (graph[*e_it].pedestrian) length += edgeLength;
        }
        else if(networkType == "residential")
        {
    		if (graph[*e_it].residential) length += edgeLength;
        }
        else if(networkType == "service")
    	{
    		if (graph[*e_it].service) length += edgeLength;
    	}
    }

    return length;
}


/////////////////////////////////////////////////////////////////////
/// Generates randomly GPS locations on a graph edge
/// @param[in] graph Undirected boost graph
/// @param[in] edge Undirected boost graph edge descriptor
/// @param[out] locations List of GPS locations
/////////////////////////////////////////////////////////////////////
int CityImp::GenerateRandomLocationsFromEdge(const OSMUndirectedGraph& graph, const OSMUndirectedGraph::edge_descriptor& edge,
											std::vector<gps>& locations)
{
	int nbOfLocations = 0;
	int interSpace = 5; // sampling every 5 meters

	auto source = boost::source(edge, graph);
	auto target = boost::target(edge, graph);

	auto slatitude  = graph[source].ylatitude;
	auto slongitude = graph[source].xlongitude;
	auto tlatitude  = graph[target].ylatitude;
	auto tlongitude = graph[target].xlongitude;


	if (graph[edge].rawgeometry.find("LINESTRING") != std::string::npos)
	{
		std::string line = graph[edge].rawgeometry;
		int parPosition = line.find("(")+1;
		line = line.substr(parPosition, line.size()-parPosition-1); // removing LINESTRING()

		std::istringstream ss(line);

		// extracting long/lat pairs
		std::vector<std::vector<double>> pairs;
		while (ss)
		{
			std::string rawpair;
			std::vector<double> paire;
			if (!std::getline(ss, rawpair, ',')) break;
			if (rawpair[0] == ' ') rawpair = rawpair.substr(1,rawpair.size()); // minor gitch fix
			// converting std::string pair to std::vector<double>(2)
			std::istringstream sss(rawpair);
			while (sss)
			{
				std::string rawCoordinate;
				if (!std::getline(sss, rawCoordinate, ' ')) break;
				paire.push_back(Utils::stodpre(rawCoordinate, 16));
			}
			assert(paire.size()==2);
			pairs.push_back(paire);
		}

		// Generating samples according to edge geometry
		for(size_t i=0; i<pairs.size()-1; ++i)
		{
			double distance = Utils::DistanceFromGPS(pairs[i][1], pairs[i][0], pairs[i+1][1], pairs[i+1][0])*1000; // in meters
			int nbOfSamples = int(distance/interSpace+0.5);
			int cpt = 0;
			while(cpt < nbOfSamples)
			{
				double randLat  = Utils::fRand(pairs[i][1], pairs[i+1][1]);
				double randLong = Utils::fRand(pairs[i][0], pairs[i+1][0]);
				locations.push_back({randLong, randLat});
				++cpt;
				++nbOfLocations;
			}
		}
	}
	else // no geometry or straight road
	{
		double distance = Utils::DistanceFromGPS(slatitude, slongitude, slatitude, tlongitude)*1000; // in meters
		int nbOfSamples = int(distance/interSpace+0.5);
		int cpt = 0;
		while(cpt < nbOfSamples)
		{
			double randLat  = Utils::fRand(slatitude, tlatitude);
			double randLong = Utils::fRand(slongitude, tlongitude);
			locations.push_back({randLong, randLat});
			++cpt;
			++nbOfLocations;
		}
	}

	return nbOfLocations;
}
