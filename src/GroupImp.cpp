#include "GroupImp.hpp"
#include "Solver.hpp"


///////////////////////////////////////////////////////////////////
///
///////////////////////////////////////////////////////////////////
GroupImp::GroupImp() :  _HW(),
						_initialListOfAgents(),
						_listOfAgents(),
						_previousListOfAgents(),
						Group()
{

}


/////////////////////////////////////////////////////////////////////
/// @param[in] homes List of home locations
/// @param[in] homes List of work locations
/// @param[in] population Population size
/////////////////////////////////////////////////////////////////////
void GroupImp::init(const std::vector<gps>& homes, const std::vector<gps>& works)
{
	_listOfAgents.clear();
	_previousListOfAgents.clear();
	_initialListOfAgents.clear();

	int agentId = 0;
	srand(time(NULL));
	while (agentId < n)
	{
		// Creating agent and assign it random addresses
		Agent a("smith" + std::to_string(agentId), homes[rand()%homes.size()], works[rand()%works.size()]);

		_listOfAgents.push_back(a);
		++agentId;
	}

	// Saving initial agents
	_initialListOfAgents = _listOfAgents;
}

/////////////////////////////////////////////////////////////////////
///
/// @return List of Agents
/////////////////////////////////////////////////////////////////////
std::vector<Agent> GroupImp::getList()
{
	return _listOfAgents;
}


/////////////////////////////////////////////////////////////////////
/// Overwrite the current population with the new one
/// @param[in] list List of Agents
/////////////////////////////////////////////////////////////////////
void GroupImp::setList(const std::vector<Agent>& list)
{
	_listOfAgents = list;
}


/////////////////////////////////////////////////////////////////////
/// @return Agent data
/// @param[in] name The agent name
/////////////////////////////////////////////////////////////////////
Agent GroupImp::getAgent(const std::string& name)
{
	int index = std::distance(_listOfAgents.begin(),
							std::find_if(_listOfAgents.begin(), _listOfAgents.end(), [&name] (const Agent& a) {return a.name == name;}));
	return _listOfAgents[index];
}

/////////////////////////////////////////////////////////////////////
/// Removes an agent from the population
/// @param[in] name Name of the agent
/////////////////////////////////////////////////////////////////////
void GroupImp::removeAgent(const std::string& name)
{
	_listOfAgents.erase(std::remove_if(_listOfAgents.begin(), _listOfAgents.end(), [&name] (const Agent& a) {return a.name == name;}));
}

/////////////////////////////////////////////////////////////////////
/// Adds an agent from the population
/// @param[in] agent All agent's data
/////////////////////////////////////////////////////////////////////
void GroupImp::addAgent(const Agent& agent)
{
	_listOfAgents.push_back(agent);
}

/////////////////////////////////////////////////////////////////////
/// Resets all population to initial state
/////////////////////////////////////////////////////////////////////
void GroupImp::reset()
{
	_listOfAgents = _initialListOfAgents;		// Reintializing agents
	_HW.init();									// 		,,		 Home-work data layer
}

/////////////////////////////////////////////////////////////////////
/// @brief Removing agents with corrupted/incomplete data. Also performs data reformatting
///
/////////////////////////////////////////////////////////////////////
void GroupImp::clean(std::vector<Agent>& list)
{
	// removing incomplete agents
	list.erase(std::remove_if(list.begin(), list.end(), [] (const Agent& a) {return a.error;}), list.end());
#ifdef DEBUG_GROUP
	std::cout << list.size() << " agents left after cleaning." << std::endl;
#endif
	// formatting addresses (removing commas)
	std::for_each(list.begin(), list.end(), [](Agent& a){
		a.homeaddress.erase(std::remove(a.homeaddress.begin(), a.homeaddress.end(), ','), a.homeaddress.end());
		a.workaddress.erase(std::remove(a.workaddress.begin(), a.workaddress.end(), ','), a.workaddress.end());
	});

	setList(list);
}


/////////////////////////////////////////////////////////////////////
/// Updates population
/// @param[in] Problem [ASSIGNMENT]
/// @param[in] Measure [TIME/DISTANCE]
/// @param[in] Layer [HW/HM/MW]
/////////////////////////////////////////////////////////////////////
void GroupImp::update(const ProblemFlag pb,
					    const MeasureFlag mflag,
					    const LayerFlag iflag)
{
	_previousListOfAgents = _listOfAgents;

	if (mflag == MeasureFlag::TIME)
	{
		if (iflag == LayerFlag::HW)
		{
			if (pb == ProblemFlag::ASSIGNMENT) Solver::Assign(&_HW.timeCostMatrix, _listOfAgents);
		}
	}
	else if (mflag == MeasureFlag::DISTANCE)
	{
		if (iflag == LayerFlag::HW)
		{
			if (pb == ProblemFlag::ASSIGNMENT) Solver::Assign(&_HW.distanceCostMatrix, _listOfAgents);
		}
	}
	else std::cerr << "Invalid Population::MeasureFlag" << std::endl;
}

/////////////////////////////////////////////////////////////////////
///	Sets up cost matrices (ex: Typically, extracted from google maps APIs)
/// @param[in] matrix Commute Cost matrix
/// @param[in] mflag  Measure (TIME/DISTANCE)
/// @param[in] Layer (HW/HM/WM)
/////////////////////////////////////////////////////////////////////
void GroupImp::setCostMatrix(const std::vector<std::vector<cost>>& matrix,
								const MeasureFlag mflag, const LayerFlag iflag)
{
	if (mflag == MeasureFlag::TIME)
	{
		if (iflag == LayerFlag::HW) _HW.timeCostMatrix = matrix;
	}
	else if (mflag == MeasureFlag::DISTANCE)
	{
		if (iflag == LayerFlag::HW) _HW.distanceCostMatrix = matrix;
	}
	else std::cerr << "Invalid Population::MeasureFlag" << std::endl;
}

/////////////////////////////////////////////////////////////////////
/// @param[in] mflag  Measure (TIME/DISTANCE)
/// @param[in] Layer (HW/HM/WM)
/// @return Commute distance cost matrix
/////////////////////////////////////////////////////////////////////
std::vector<std::vector<cost>> 	GroupImp::getCostMatrix(const MeasureFlag mflag, const LayerFlag iflag)
{
	if (mflag == MeasureFlag::TIME)
	{
		if (iflag == LayerFlag::HW) return _HW.timeCostMatrix;
	}
	else if (mflag == MeasureFlag::DISTANCE)
	{
		if (iflag == LayerFlag::HW) return _HW.distanceCostMatrix;
	}
	else std::cerr << "Invalid Population::MeasureFlag" << std::endl;

	return {};
}

