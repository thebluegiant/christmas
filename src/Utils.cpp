#include <set>
#include <opencv2/imgproc/imgproc.hpp>
#include "colors.hpp"
#include "Utils.hpp"


// TOOLS
//

/////////////////////////////////////////////////////////////////////
/// Computes distance between 2 GPS locations
/// @param[in] lat1d latitude of location 1
/// @param[in] lon1d longitude of location 1
/// @param[in] lat2d latitude of location 2
/// @param[in] lon2d longitude of location 2
/// @return Distance in Kms
/////////////////////////////////////////////////////////////////////
double Utils::DistanceFromGPS (const double lat1d, const double lon1d, const double lat2d, const double lon2d)
{
	double lat1r = Deg2Rad(lat1d);
	double lon1r = Deg2Rad(lon1d);
	double lat2r = Deg2Rad(lat2d);
	double lon2r = Deg2Rad(lon2d);
	double u = sin((lat2r - lat1r)/2);
	double v = sin((lon2r - lon1r)/2);
	return 2.0 * EARTH_RADIUS_KM * asin(sqrt(u * u + cos(lat1r) * cos(lat2r) * v * v));
}


/////////////////////////////////////////////////////////////////////
/// Draws 2 points and binds them with a line
/// @param[out] map Opencv map
/// @param[in] p1 Opencv integer point
/// @param[in] p2 Opencv integer point
/// @param[in] linecolor RGB color
/////////////////////////////////////////////////////////////////////
void Utils::DrawPair(cv::Mat& map, const cv::Point& p1, const cv::Point& p2, cv::Scalar& linecolor)
{
	cv::circle(map, p1, 2, cv::Scalar(0, 140, 255), 4);
	cv::circle(map, p2, 2, black, 4);
	cv::line(map, p1, p2, linecolor, 1);
}

/////////////////////////////////////////////////////////////////////
/// Checks if a candidate point is inside a triangle or not
/// @param[in] pt Candidate floating point
/// @param[in] v1 First floating point
/// @param[in] v2 Second floating point
/// @param[in] v3 Third floating point
/// @return true / false
/////////////////////////////////////////////////////////////////////
bool Utils::PointInTriangle (const cv::Point2f& pt, const cv::Point2f& v1, const cv::Point2f& v2, const cv::Point2f& v3)
{
	bool b1, b2, b3;

	b1 = Sign(pt, v1, v2) < 0.0f;
	b2 = Sign(pt, v2, v3) < 0.0f;
	b3 = Sign(pt, v3, v1) < 0.0f;

	return ((b1 == b2) && (b2 == b3));
}


/////////////////////////////////////////////////////////////////////
/// Fills triangle with a given color
/// @param[out] frame Opencv map
/// @param[in] triangle Triangle coordinates
/// @param[in] color RGB Color
/////////////////////////////////////////////////////////////////////
void Utils::FillTriangle(cv::Mat& frame, const cv::Vec6f& triangle, const cv::Scalar& color)
{
	cv::Point tmpNodes [1][3] = {cv::Point(cvRound(triangle[0]), cvRound(triangle[1])),
								cv::Point(cvRound(triangle[2]), cvRound(triangle[3])),
								cv::Point(cvRound(triangle[4]), cvRound(triangle[5]))};
	int npt[] = { 3 };
	const cv::Point* nodes[1] = { tmpNodes[0] };
	cv::fillPoly(frame, nodes, npt, 1, color);
}


/////////////////////////////////////////////////////////////////////
/// Pixels in a given color are set to white, the rest to black.
/// @param[in] image Opencv RGB map
/// @param[out] binarized Opencv single channel map
/// @param[in] value RGB Color
/////////////////////////////////////////////////////////////////////
void Utils::BinarizeByValue (const cv::Mat& image, cv::Mat& binarized, const cv::Scalar& value)
{
	binarized.create(image.size(), CV_8UC1);
	std::fill(&binarized.data[0], &binarized.data[binarized.rows*binarized.cols], 0);

	for (size_t i = 0; i < binarized.cols; ++i)
		for (size_t j = 0; j < binarized.rows; ++j)
			if (image.at<cv::Vec3b>(j, i).val[0] == value.val[0] &&
				image.at<cv::Vec3b>(j, i).val[1] == value.val[1] &&
				image.at<cv::Vec3b>(j, i).val[2] == value.val[2])
				binarized.data[j*binarized.cols + i] = (int) 255; // white
}

/////////////////////////////////////////////////////////////////////
/// Pixels in colors present in redZonesColors are set to black, the rest to white.
///	The blacked pixels are then submitted to morphological operations.
/// @param[in] toBinarize Opencv RGB map
/// @param[out] binarized Opencv single channel map
/// @param[in] redZonesColors RGB Colors
/////////////////////////////////////////////////////////////////////
void Utils::BinarizeMap(const cv::Mat& toBinarize, cv::Mat& binarized, const std::vector<cv::Scalar>& redZonesColors)
{
	binarized.create(cv::Size(toBinarize.cols, toBinarize.rows), CV_8UC1);
	std::fill(&binarized.data[0], &binarized.data[binarized.rows*binarized.cols], 255);

	for (int i = 0; i < binarized.cols; ++i)
		for (int j = 0; j < binarized.rows; ++j)
			for (int k = 0; k < redZonesColors.size(); ++k)
				if (toBinarize.at<cv::Vec3b>(j, i).val[0] == redZonesColors[k].val[0] &&
					toBinarize.at<cv::Vec3b>(j, i).val[1] == redZonesColors[k].val[1] &&
					toBinarize.at<cv::Vec3b>(j, i).val[2] == redZonesColors[k].val[2])
				{
					binarized.data[j*binarized.cols + i] = (int)0; // black
					continue;
				}

	int erosion_type = cv::MORPH_RECT;
	cv::Mat element = cv::getStructuringElement(erosion_type, cv::Size(13, 13));
	cv::erode(binarized, binarized, element);

	int ccols = binarized.cols;
	int rrows = binarized.rows;

	// for google maps screenshots / removing bottom right details
	cv::Mat roi1 = binarized(cv::Rect(ccols - ccols / 20, rrows - rrows / 5, ccols / 20, rrows / 5));
	cv::Mat roi2 = binarized(cv::Rect(ccols - ccols / 3, rrows - rrows / 10, ccols / 3, rrows / 10));
	roi1.setTo(cv::Scalar(0));
	roi2.setTo(cv::Scalar(0));
}

/////////////////////////////////////////////////////////////////////
/// Sets area of an image to black
/// @param[in][out] map Opencv map
/// @param[in] zone RoI
/////////////////////////////////////////////////////////////////////
void Utils::ClearArea(cv::Mat& map, cv::Rect zone)
{
	if (zone.width == 0) map.setTo(black);
	else map(cv::Rect(zone.x+1, zone.y+1, zone.width-1, zone.height-1)).setTo(black);
}

/////////////////////////////////////////////////////////////////////
/// Checks on which side of the half-plane created by the edges the point is
/// @param[in] p  Candidate floating point
/// @param[in] p1 First floating point
/// @param[in] p2 Second floating point
/////////////////////////////////////////////////////////////////////
float Utils::Sign (const cv::Point2f& p, const cv::Point2f& p1, const cv::Point2f& p2)
{
	return (p.x - p2.x) * (p1.y - p2.y) - (p1.x - p2.x) * (p.y - p2.y);
}

double Utils::fRand(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

double Utils::stodpre(std::string const &str, std::size_t const p)
{
	std::stringstream sstrm;
	sstrm << std::setprecision(p) << std::fixed << str << std::endl;

	double d;
	sstrm >> d;

	return d;
}
