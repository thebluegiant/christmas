#include "CityImp2.hpp"
#include <boost/filesystem.hpp>

#include <osmium/io/any_input.hpp>
#include <osmium/geom/haversine.hpp>
#include <osmium/visitor.hpp>
#include <osmium/index/map/flex_mem.hpp>
#include <osmium/handler/node_locations_for_ways.hpp>


using index_type = osmium::index::map::FlexMem<osmium::unsigned_object_id_type, osmium::Location>;
using location_handler_type = osmium::handler::NodeLocationsForWays<index_type>;
struct RoadLengthHandler : public osmium::handler::Handler
{
    double length = 0;

    void way(const osmium::Way& way)
    {
        const char* highway = way.tags()["highway"];
        if (highway) length += osmium::geom::haversine::distance(way.nodes());
    }

};

CityImp2::CityImp2(const std::string& file) : City()
{
	std::string ext = boost::filesystem::extension(file);
	if(ext != ".osm" && ext != ".o5m")
	{
		std::cerr << "This implementation only supports xml files (o5m, osm)." << std::endl;
		return;
	}

	osmium::io::Reader reader{file, osmium::osm_entity_bits::node | osmium::osm_entity_bits::way};

    index_type index; // holds node locations
    location_handler_type location_handler{index}; 				  // Adds the node locations to the index and then to the ways
    RoadLengthHandler road_length_handler; 						  // Our handler defined above
    osmium::apply(reader, location_handler, road_length_handler); // Apply input data to first the location handler and then our own handler

    std::cout << "Length: " << road_length_handler.length / 1000 << " km\n";

    reader.close();

//	_nbOfIntersections = boost::num_vertices(_osmCityUndirectedGraph);
//	_nbOfRoads = boost::num_edges(_osmCityUndirectedGraph);
//
//	_avgLatitude  = ComputeAvgLatitude(_osmCityUndirectedGraph);
//	_avgLongitude = ComputeAvgLongitude(_osmCityUndirectedGraph);
//	_totalNetLength = ComputeTypeNetLength(_osmCityUndirectedGraph); // all_private
//	_bikeNetLength  = ComputeTypeNetLength(_osmCityUndirectedGraph, "cycleway");
//	_pedestrianNetLength = 	ComputeTypeNetLength(_osmCityUndirectedGraph, "pedestrian");
//	_bikableNetLength = ComputeTypeNetLength(_osmCityUndirectedGraph, "bikable");
//	_walkableNetLength  = ComputeTypeNetLength(_osmCityUndirectedGraph, "walkable");
//	_drivableNetLength  = ComputeTypeNetLength(_osmCityUndirectedGraph, "drivable");
//	_residentialNetLength = ComputeTypeNetLength(_osmCityUndirectedGraph, "residential");
//	_serviceNetLength = 	ComputeTypeNetLength(_osmCityUndirectedGraph, "service");
//
//	std::cout << "Lat/Long: " << _avgLatitude << " / " << _avgLongitude << " & ";
//	std::cout << "Nodes/Edges: " << _nbOfIntersections << " / " << _nbOfRoads << std::endl;
}
