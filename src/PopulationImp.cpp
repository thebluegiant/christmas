#include "PopulationImp.hpp"

//
//

PopulationImp::PopulationImp(const int n)
{

}


/////////////////////////////////////////////////////////////////////
///
///
/////////////////////////////////////////////////////////////////////
void PopulationImp::addGroup(const std::shared_ptr<Group>& g, const std::string& name)
{
	_pop.insert({name, g});
}

/////////////////////////////////////////////////////////////////////
///
///
/////////////////////////////////////////////////////////////////////
void PopulationImp::removeGroup(const std::string& name)
{
	if (_pop.erase(name) == 1) std::cout << "Group " << name << " removed." << std::endl;
	else std::cout << "Group " << name << " not removed." << std::endl;
}

/////////////////////////////////////////////////////////////////////
///
///
/////////////////////////////////////////////////////////////////////
std::shared_ptr<Group> PopulationImp::getGroup (const std::string& name)
{
	auto obj = _pop.find(name);
	if (obj == _pop.end())
	{
		std::cerr << "Couldn't find Group " << name << std::endl;
		return nullptr;
	}

	return obj->second;
}
