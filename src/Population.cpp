#include "Population.hpp"
#include "PopulationImp.hpp"

/////////////////////////////////////////////////////////////////////
///
///
/////////////////////////////////////////////////////////////////////
std::shared_ptr<Population> Population::Create(const int n)
{
	return std::make_shared<PopulationImp>(n);
}

