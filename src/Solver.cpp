#include "Solver.hpp"
#include "Agent.hpp"
#include "Algorithms.hpp"


/////////////////////////////////////////////////////////////////////
/// Runs assignment algorithm
/// @param[in] costMatrix Square cost matrix
/// @return {total old cost, total new cost}
/////////////////////////////////////////////////////////////////////
std::pair<cost, cost> Solver::Assign(std::vector<std::vector<cost>>* costMatrix, std::vector<Agent>& agentList)
{
	assert(costMatrix != 0);

	int population = agentList.size();
	std::vector<row> rowAssignment(population);
	std::vector<col> colAssignment(population);

	// Computing original cost
	cost ocost = 0;
	for (size_t i=0; i<population; ++i) ocost += (*costMatrix)[i][i]; // original total cost = matrix trace

	// running assignment
	cost costAssignement = Algorithms::LapJV(*costMatrix, rowAssignment, colAssignment);

	// assigning
	for (size_t i=0; i<population; ++i)
	{
		std::swap(agentList[i].homelocation, agentList[rowAssignment[i]].homelocation);
		std::swap(agentList[i].homeaddress, agentList[rowAssignment[i]].homeaddress);
	}

#ifndef DEBUG_SOLVER
	std::cout << "Old total cost = " << ocost << std::endl;
	std::cout << "New total cost = " << costAssignement << std::endl;
	std::cout << "Represents  " << (costAssignement * 100) / ocost << " % of original cost." << std::endl;
#endif

	return {ocost, costAssignement};
}
